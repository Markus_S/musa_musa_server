package com.myqnapcloud.sonata.musa.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 * Persistent album entity with JPA markup. Albums are stored in a MySQL
 * relational database.
 * 
 * @author Markus Sairanen
 */
@Entity
@Table(name="Album")
public class Album implements Serializable {

	private static final long serialVersionUID = -7020994966529821790L;
	
	@Id
	private Long id;
	@ManyToMany(fetch = FetchType.EAGER)
	private Set<Artist> artist;
	private String name;
	private int tracks = 0;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Set<Artist> getArtist() {
		return artist;
	}

	public void setArtist(Set<Artist> artist) {
		this.artist = artist;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getTracks() {
		return tracks;
	}

	public void setTracks(int tracks) {
		this.tracks = tracks;
	}

	@Override
	public String toString() {
		return "Album [albumId=" + id + ", artist=" + artist + ", name=" + name + ", tracks=" + tracks + "]";
	}
}
