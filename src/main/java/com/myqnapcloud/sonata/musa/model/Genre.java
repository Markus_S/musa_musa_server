package com.myqnapcloud.sonata.musa.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Persistent genre entity with JPA markup. Genres are stored in a MySQL
 * relational database.
 * 
 * @author Markus Sairanen
 */
@Entity
@Table(name = "Genre")
public class Genre implements Serializable {

	private static final long serialVersionUID = -5658027986934605123L;
	
	@Id
	private Long id;
	private String name;
	private int usageCount;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getUsageCount() {
		return usageCount;
	}
	public void setUsageCount(int usageCount) {
		this.usageCount = usageCount;
	}
	@Override
	public String toString() {
		return "Genre [id=" + id + ", name=" + name + ", usageCount=" + usageCount + "]";
	}
}
