package com.myqnapcloud.sonata.musa.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Persistent song entity with JPA markup. Songs are stored in a MySQL
 * relational database.
 * 
 * @author Markus Sairanen
 */
@Entity
@Table(name = "Song")
public class Song implements Serializable {
	
	private static final long serialVersionUID = -7796664199876288022L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@ManyToMany(fetch = FetchType.EAGER)
	private Set<Artist> artist;
	@ManyToOne
	@JoinColumn(name = "albumId")
	private Album album;
	private int trackNumber;
	private String songTitle;
	private int year;
	@ManyToMany(fetch = FetchType.EAGER)
	private Set<Genre> genre;
	private int songLength;
	@Column(name = "f_song_length")
	private String fSongLength;
	private int bitRate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Set<Artist> getArtist() {
		return artist;
	}

	public void setArtist(Set<Artist> artist) {
		this.artist = artist;
	}

	public Album getAlbum() {
		return album;
	}

	public void setAlbum(Album album) {
		this.album = album;
	}

	public int getTrackNumber() {
		return trackNumber;
	}

	public void setTrackNumber(int trackNumber) {
		this.trackNumber = trackNumber;
	}

	public String getSongTitle() {
		return songTitle;
	}

	public void setSongTitle(String songTitle) {
		this.songTitle = songTitle;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public Set<Genre> getGenre() {
		return genre;
	}

	public void setGenre(Set<Genre> genre) {
		this.genre = genre;
	}

	public int getSongLength() {
		return songLength;
	}

	public void setSongLength(int songLength) {
		this.songLength = songLength;
	}
	
	public String getFSongLength() {
		return fSongLength;
	}

	public void setFSongLength(String fSongLength) {
		this.fSongLength = fSongLength;
	}

	public int getBitRate() {
		return bitRate;
	}

	public void setBitRate(int bitRate) {
		this.bitRate = bitRate;
	}

	@Override
	public String toString() {
		return "Song [id=" + id + ", artist=" + artist + ", album=" + album + ", trackNumber=" + trackNumber
				+ ", songTitle=" + songTitle + ", year=" + year + ", genre=" + genre + ", songLength=" + songLength
				+ ", bitRate=" + bitRate + "]";
	}
}
