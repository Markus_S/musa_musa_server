package com.myqnapcloud.sonata.musa.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Persistent artist entity with JPA markup. Artists are stored in a MySQL
 * relational database.
 * 
 * @author Markus Sairanen
 */
@Entity
@Table(name = "Artist")
public class Artist implements Serializable {

	private static final long serialVersionUID = -3388717261321088765L;
	
	@Id
	private Long id;
	private String name;
	private int tracks;
	private int albums;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getTracks() {
		return tracks;
	}

	public void setTracks(int tracks) {
		this.tracks = tracks;
	}

	public int getAlbums() {
		return albums;
	}

	public void setAlbums(int albums) {
		this.albums = albums;
	}

	@Override
	public String toString() {
		return "Artist [id=" + id + ", name=" + name + ", tracks=" + tracks + ", albums=" + albums + "]";
	}
}
