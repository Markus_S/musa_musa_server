package com.myqnapcloud.sonata.musa.exceptions;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

/**
 * Allow the controller to return a 404 if an account is not found by simply
 * throwing this exception. The @ResponseStatus causes Spring MVC to return a
 * 404 instead of the usual 500.
 * 
 * @author Markus Sairanen
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class SongNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public SongNotFoundException(String songName) {
		super("No such song: " + songName);
	}
	
	public SongNotFoundException(Long songId) {
		super("No such song with ID: " + songId);
	}
}
