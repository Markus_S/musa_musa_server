package com.myqnapcloud.sonata.musa.service;

import java.util.List;
import java.util.stream.Stream;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.Repository;

import com.myqnapcloud.sonata.musa.model.Album;

/**
 * Repository for Album data implemented using Spring Data JPA.
 * 
 * @author Markus Sairanen
 */
public interface AlbumRepository extends Repository<Album, Long> {
	Album findOne(Long id);
	Stream<Album> findAll();
	List<Album> findByNameIgnoreCase(String name);
	Page<Album> findByNameStartsWithIgnoreCaseOrderByName(String firstLetter, Pageable pageable);
	Page<Album> findByNameStartsWithOrNameStartsWithOrNameStartsWithOrNameStartsWithOrNameStartsWithOrNameStartsWithOrNameStartsWithOrNameStartsWithOrNameStartsWithOrNameStartsWithIgnoreCaseOrderByName(String a, String b, String c, String d, String e, String f, String g, String h, String i, String j, Pageable pageable);
	List<Album> findByArtistId(Long id);
	List<Album> findByArtistNameIgnoreCase(String name);
	boolean exists(Long id);
	long count();
}
