package com.myqnapcloud.sonata.musa.service;

import java.util.List;
import java.util.stream.Stream;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.Repository;

import com.myqnapcloud.sonata.musa.model.Song;

public interface SongRepository extends Repository<Song, Long> {
	Stream<Song> findAll();
	List<Song> findByAlbumIdOrderByTrackNumber(Long id);
	List<Song> findByAlbumNameIgnoreCase(String name);
	List<Song> findByArtistId(Long id);
	List<Song> findByArtistNameIgnoreCase(String name);
	Page<Song> findByGenreIdOrderByAlbumIdAscTrackNumberAsc(Long id, Pageable pageable);
	List<Song> findByGenreNameIgnoreCase(String name);
	Page<Song> findByYearOrderByAlbumIdAscTrackNumberAsc(int year, Pageable pageable);
	Page<Song> findBySongTitleStartsWithIgnoreCaseOrderBySongTitle(String firstLetter, Pageable pageable);
	Page<Song> findBySongTitleStartsWithOrSongTitleStartsWithOrSongTitleStartsWithOrSongTitleStartsWithOrSongTitleStartsWithOrSongTitleStartsWithOrSongTitleStartsWithOrSongTitleStartsWithOrSongTitleStartsWithOrSongTitleStartsWithIgnoreCaseOrderBySongTitle(String a, String b, String c, String d, String e, String f, String g, String h, String i, String j, Pageable pageable);
	boolean exists(Long id);
	long count();
}
