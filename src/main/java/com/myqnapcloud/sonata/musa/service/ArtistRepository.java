package com.myqnapcloud.sonata.musa.service;

import java.util.stream.Stream;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.Repository;

import com.myqnapcloud.sonata.musa.model.Artist;

public interface ArtistRepository extends Repository<Artist, Long> {
	Artist findOne(Long id);
	Stream<Artist> findAll();
	Artist findFirstByNameIgnoreCase(String name);
	Page<Artist> findByNameStartsWithIgnoreCaseOrderByName(String firstLetter, Pageable pageable);
	Page<Artist> findByNameStartsWithOrNameStartsWithOrNameStartsWithOrNameStartsWithOrNameStartsWithOrNameStartsWithOrNameStartsWithOrNameStartsWithOrNameStartsWithOrNameStartsWithIgnoreCaseOrderByName(String a, String b, String c, String d, String e, String f, String g, String h, String i, String j, Pageable pageable);
	boolean exists(Long id);
	long count();
}
