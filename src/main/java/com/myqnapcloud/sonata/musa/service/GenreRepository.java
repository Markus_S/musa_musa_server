package com.myqnapcloud.sonata.musa.service;

import java.util.stream.Stream;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.Repository;

import com.myqnapcloud.sonata.musa.model.Genre;

public interface GenreRepository extends Repository<Genre, Long> {
	Genre findOne(Long id);
	Stream<Genre> findAll();
	Genre findFirstByNameIgnoreCase(String name);
	Page<Genre> findByNameStartsWithIgnoreCaseOrderByName(String firstLetter, Pageable pageable);
	Page<Genre> findByNameStartsWithOrNameStartsWithOrNameStartsWithOrNameStartsWithOrNameStartsWithOrNameStartsWithOrNameStartsWithOrNameStartsWithOrNameStartsWithOrNameStartsWithIgnoreCaseOrderByName(String a, String b, String c, String d, String e, String f, String g, String h, String i, String j, Pageable pageable);
	boolean exists(Long id);
	long count();
}
