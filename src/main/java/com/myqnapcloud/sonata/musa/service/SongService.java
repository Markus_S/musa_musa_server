package com.myqnapcloud.sonata.musa.service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.myqnapcloud.sonata.musa.model.Song;

@Service
@Transactional
public class SongService extends GenericService {
	private final SongRepository songRepository;
	private final static Logger logger = LoggerFactory.getLogger(SongService.class);

	@Autowired
	public SongService(SongRepository songRepository) {
		this.songRepository = songRepository;
	}

	/**
	 * Find all songs.
	 *
	 * @return All songs if found, empty list otherwise.
	 */
	public List<Song> findAll() {
		try(Stream<Song> songs = songRepository.findAll()) {
			return songs.collect(Collectors.toList());
		}
	}
	
	/**
	 * Find songs with the specified album id.
	 *
	 * @param id
	 * @return The songs if found, empty list otherwise.
	 */
	public List<Song> findByAlbumId(Long id) {
		return songRepository.findByAlbumIdOrderByTrackNumber(id);
	}
	
	/**
	 * Find songs with the specified album name.
	 *
	 * @param name
	 * @return The songs if found, empty list otherwise.
	 */
	public List<Song> findByAlbumName(String name) {
		return songRepository.findByAlbumNameIgnoreCase(name);
	}
	
	/**
	 * Find songs starting with the specified letter.
	 *
	 * @param firstLetter
	 * @param pageNumber
	 * @return The songs if found, empty list otherwise.
	 */
	public Page<Song> findByFirstLetter(String firstLetter, int pageNumber) {
		PageRequest request = new PageRequest(pageNumber - 1, RESULTS_IN_PAGE);
		
		if (firstLetter.equals("0")) {
			return songRepository.findBySongTitleStartsWithOrSongTitleStartsWithOrSongTitleStartsWithOrSongTitleStartsWithOrSongTitleStartsWithOrSongTitleStartsWithOrSongTitleStartsWithOrSongTitleStartsWithOrSongTitleStartsWithOrSongTitleStartsWithIgnoreCaseOrderBySongTitle("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", request);
		}
		
		return songRepository.findBySongTitleStartsWithIgnoreCaseOrderBySongTitle(firstLetter, request);
	}
	
	/**
	 * Find songs with the specified artist id.
	 *
	 * @param id
	 * @return The songs if found, empty list otherwise.
	 */
	public List<Song> findByArtistId(Long id) {
		return songRepository.findByArtistId(id);
	}
	
	/**
	 * Find songs with the specified artist name.
	 *
	 * @param name
	 * @return The songs if found, empty list otherwise.
	 */
	public List<Song> findByArtistName(String name) {
		return songRepository.findByArtistNameIgnoreCase(name);
	}
	
	/**
	 * Find songs with the specified genre id.
	 *
	 * @param id
	 * @param pageNumber 
	 * @return The songs if found, empty list otherwise.
	 */
	public Page<Song> findByGenreId(Long id, int pageNumber) {
		PageRequest request = new PageRequest(pageNumber - 1, RESULTS_IN_PAGE);
		
		return songRepository.findByGenreIdOrderByAlbumIdAscTrackNumberAsc(id, request);
	}
	
	/**
	 * Find songs with the specified genre name.
	 *
	 * @param name
	 * @return The songs if found, empty list otherwise.
	 */
	public List<Song> findByGenreName(String name) {
		return songRepository.findByGenreNameIgnoreCase(name);
	}
	
	/**
	 * Find songs with the specified year.
	 *
	 * @param year
	 * @return The songs if found, empty list otherwise.
	 */
	public Page<Song> findByYear(int year, int pageNumber) {
		PageRequest request = new PageRequest(pageNumber - 1, RESULTS_IN_PAGE);
		
		return songRepository.findByYearOrderByAlbumIdAscTrackNumberAsc(year, request);
	}
	
	/**
	 * Find how many songs there are in the DB.
	 *
	 * @return Song count.
	 */
	public long count() {
		return songRepository.count();
	}

	/**
	 * Check if song with the specified number exists.
	 *
	 * @param id
	 * @return True if the song exists, false otherwise.
	 */
	public boolean exists(Long id) {
		return songRepository.exists(id);
	}
}
