package com.myqnapcloud.sonata.musa.service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.myqnapcloud.sonata.musa.model.Artist;

@Service
@Transactional
public class ArtistService extends GenericService {
	private final ArtistRepository artistRepository;
	private final static Logger logger = LoggerFactory.getLogger(ArtistService.class);

	@Autowired
	public ArtistService(ArtistRepository artistRepository) {
		this.artistRepository = artistRepository;
	}

	/**
	 * Find an artist with the specified artist number.
	 *
	 * @param id
	 * @return The artist if found, null otherwise.
	 */
	public Artist findById(Long id) {
		return artistRepository.findOne(id);
	}

	/**
	 * Find all artists.
	 *
	 * @return All artists if found, empty list otherwise.
	 */
	public List<Artist> findAll() {
		try(Stream<Artist> artists = artistRepository.findAll()) {
			return artists.collect(Collectors.toList());
		}
	}
	
	/**
	 * Find an artist with the specified artist name.
	 *
	 * @param name
	 * @return The artist if found, null otherwise.
	 */
	public Artist findByname(String name) {
		return artistRepository.findFirstByNameIgnoreCase(name);
	}
	
	/**
	 * Find artists starting with the specified letter.
	 *
	 * @param firstLetter
	 * @param pageNumber
	 * @return The artists if found, empty list otherwise.
	 */
	public Page<Artist> findByFirstLetter(String firstLetter, int pageNumber) {
		PageRequest request = new PageRequest(pageNumber - 1, RESULTS_IN_PAGE);
		
		if (firstLetter.equals("0")) {
			return artistRepository.findByNameStartsWithOrNameStartsWithOrNameStartsWithOrNameStartsWithOrNameStartsWithOrNameStartsWithOrNameStartsWithOrNameStartsWithOrNameStartsWithOrNameStartsWithIgnoreCaseOrderByName("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", request);
		}
		
		return artistRepository.findByNameStartsWithIgnoreCaseOrderByName(firstLetter, request);
	}
	
	/**
	 * Find how many artists there are in the DB.
	 *
	 * @return Artist count.
	 */
	public long count() {
		return artistRepository.count();
	}

	/**
	 * Check if artist with the specified number exists.
	 *
	 * @param id
	 * @return True if the artist exists, false otherwise.
	 */
	public boolean exists(Long id) {
		return artistRepository.exists(id);
	}
}
