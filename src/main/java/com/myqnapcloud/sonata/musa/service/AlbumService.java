package com.myqnapcloud.sonata.musa.service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.myqnapcloud.sonata.musa.model.Album;

@Service
@Transactional
public class AlbumService extends GenericService {
	private final AlbumRepository albumRepository;
	private final static Logger logger = LoggerFactory.getLogger(AlbumService.class);

	@Autowired
	public AlbumService(AlbumRepository albumRepository) {
		this.albumRepository = albumRepository;
	}

	/**
	 * Find an album with the specified album number.
	 *
	 * @param id
	 * @return The album if found, null otherwise.
	 */
	public Album findById(Long id) {
		return albumRepository.findOne(id);
	}

	/**
	 * Find all albums.
	 *
	 * @return All albums if found, empty list otherwise.
	 */
	public List<Album> findAll() {
		try(Stream<Album> albums = albumRepository.findAll()) {
			return albums.collect(Collectors.toList());
		}
	}
	
	/**
	 * Find albums with the specified album name.
	 *
	 * @param name
	 * @return The albums if found, empty list otherwise.
	 */
	public List<Album> findByName(String name) {
		return albumRepository.findByNameIgnoreCase(name);
	}
	
	/**
	 * Find albums starting with the specified letter.
	 *
	 * @param firstLetter
	 * @param pageNumber
	 * @return The albums if found, empty list otherwise.
	 */
	public Page<Album> findByFirstLetter(String firstLetter, int pageNumber) {
		PageRequest request = new PageRequest(pageNumber - 1, RESULTS_IN_PAGE);
		
		if (firstLetter.equals("0")) {
			return albumRepository.findByNameStartsWithOrNameStartsWithOrNameStartsWithOrNameStartsWithOrNameStartsWithOrNameStartsWithOrNameStartsWithOrNameStartsWithOrNameStartsWithOrNameStartsWithIgnoreCaseOrderByName("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", request);
		}
		
		return albumRepository.findByNameStartsWithIgnoreCaseOrderByName(firstLetter, request);
	}
	
	/**
	 * Find albums with the specified artist number.
	 *
	 * @param id
	 * @return The albums if found, empty list otherwise.
	 */
	public List<Album> findByArtistId(Long id) {
		return albumRepository.findByArtistId(id);
	}
	
	/**
	 * Find albums with the specified artist name.
	 *
	 * @param name
	 * @return The albums if found, empty list otherwise.
	 */
	public List<Album> findByArtistName(String name) {
		return albumRepository.findByArtistNameIgnoreCase(name);
	}

	/**
	 * Find how many albums there are in the DB.
	 *
	 * @return Album count.
	 */
	public long count() {
		return albumRepository.count();
	}

	/**
	 * Check if album with the specified number exists.
	 *
	 * @param id
	 * @return True if the album exists, false otherwise.
	 */
	public boolean exists(Long id) {
		return albumRepository.exists(id);
	}
}
