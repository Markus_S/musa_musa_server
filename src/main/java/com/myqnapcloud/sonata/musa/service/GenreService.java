package com.myqnapcloud.sonata.musa.service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.myqnapcloud.sonata.musa.model.Genre;

@Service
@Transactional
public class GenreService extends GenericService {
	private final GenreRepository genreRepository;
	private final static Logger logger = LoggerFactory.getLogger(GenreService.class);

	@Autowired
	public GenreService(GenreRepository genreRepository) {
		this.genreRepository = genreRepository;
	}

	/**
	 * Find an genre with the specified genre number.
	 *
	 * @param id
	 * @return The genre if found, null otherwise.
	 */
	public Genre findbyId(Long id) {
		return genreRepository.findOne(id);
	}

	/**
	 * Find all genres.
	 *
	 * @return All genres if found, empty list otherwise.
	 */
	public List<Genre> findAll() {
		try(Stream<Genre> genres = genreRepository.findAll()) {
			return genres.collect(Collectors.toList());
		}
	}
	
	/**
	 * Find genre with the specified genre name.
	 *
	 * @param name
	 * @return The genre if found, null otherwise.
	 */
	public Genre findByname(String name) {
		return genreRepository.findFirstByNameIgnoreCase(name);
	}
	
	/**
	 * Find genres starting with the specified letter.
	 *
	 * @param firstLetter
	 * @param pageNumber
	 * @return The genres if found, empty list otherwise.
	 */
	public Page<Genre> findByFirstLetter(String firstLetter, int pageNumber) {
		PageRequest request = new PageRequest(pageNumber - 1, RESULTS_IN_PAGE);
		
		if (firstLetter.equals("0")) {
			return genreRepository.findByNameStartsWithOrNameStartsWithOrNameStartsWithOrNameStartsWithOrNameStartsWithOrNameStartsWithOrNameStartsWithOrNameStartsWithOrNameStartsWithOrNameStartsWithIgnoreCaseOrderByName("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", request);
		}
		
		return genreRepository.findByNameStartsWithIgnoreCaseOrderByName(firstLetter, request);
	}
	
	/**
	 * Find how many genres there are in the DB.
	 *
	 * @return Genre count.
	 */
	public long count() {
		return genreRepository.count();
	}

	/**
	 * Check if genre with the specified number exists.
	 *
	 * @param id
	 * @return True if the genre exists, false otherwise.
	 */
	public boolean exists(Long id) {
		return genreRepository.exists(id);
	}
}
