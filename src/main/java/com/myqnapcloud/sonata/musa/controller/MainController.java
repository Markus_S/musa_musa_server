package com.myqnapcloud.sonata.musa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.myqnapcloud.sonata.musa.service.AlbumService;
import com.myqnapcloud.sonata.musa.service.ArtistService;
import com.myqnapcloud.sonata.musa.service.GenreService;
import com.myqnapcloud.sonata.musa.service.SongService;

@Controller
public class MainController {
	
	private AlbumService albumService;
	private ArtistService artistService;
	private GenreService genreService;
	private SongService songService;
	
	@Autowired
	public MainController(AlbumService albumService, ArtistService artistService, GenreService genreService, SongService songService) {
		this.albumService = albumService;
		this.artistService = artistService;
		this.genreService = genreService;
		this.songService = songService;
	}

	@RequestMapping(value={"/main", "/"})
	public ModelAndView showMainpage() {
		ModelAndView mv = new ModelAndView();
		mv.addObject("message", 
			"# of albums: " + albumService.count() + ", " +
			"# of artists: " + artistService.count() + ", " +
			"# of genres: " + genreService.count() + " and " +
			"# of songs: " + songService.count()
		);
		mv.setViewName("mainPage");

		return mv;
	}
}
