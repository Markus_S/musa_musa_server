package com.myqnapcloud.sonata.musa.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.myqnapcloud.sonata.musa.exceptions.AlbumNotFoundException;
import com.myqnapcloud.sonata.musa.exceptions.ArtistNotFoundException;
import com.myqnapcloud.sonata.musa.exceptions.GenreNotFoundException;
import com.myqnapcloud.sonata.musa.exceptions.SongNotFoundException;
import com.myqnapcloud.sonata.musa.exceptions.YearNotFoundException;
import com.myqnapcloud.sonata.musa.model.Song;
import com.myqnapcloud.sonata.musa.service.SongService;

/**
 * A RESTFul controller for accessing song information.
 *
 * @author Markus Sairanen
 */
@RestController
@RequestMapping("/songs")
public class SongController {

	private SongService songService;
	private static final int DEFAULT_PAGE = 1;

	@Autowired
	public SongController(SongService songService) {
		this.songService = songService;
	}

	/**
	 * Fetch songs with the specified album name.
	 * @param albumName
	 * @return A non-null, non-empty set of songs.
	 * @throws AlbumNotFoundException
	 *             If there are no matches at all.
	 */
	@Cacheable("songs")
	@RequestMapping("/album/name/{albumName}")
	public List<Song> byAlbumName(@PathVariable("albumName") String albumName) {
		List<Song> songs = songService.findByAlbumName(albumName);

		if (songs.isEmpty()) {
			throw new AlbumNotFoundException(albumName);
		} else {
			return songs;
		}
	}

	/**
	 * Fetch songs with the specified album number.
	 * @param albumId
	 * @return A non-null, non-empty set of songs.
	 * @throws AlbumNotFoundException
	 *             If there are no matches at all.
	 */
	@Cacheable("songs")
	@RequestMapping("/album/id/{albumId}")
	public List<Song> byAlbumId(@PathVariable("albumId") Long albumId) {
		List<Song> songs = songService.findByAlbumId(albumId);

		if (songs.isEmpty()) {
			throw new AlbumNotFoundException(albumId);
		} else {
			return songs;
		}
	}

	/**
	 * Fetch songs with the specified artist name.
	 * @param artistName
	 * @return A non-null, non-empty set of songs.
	 * @throws ArtistNotFoundException
	 *             If there are no matches at all.
	 */
	@Cacheable("songs")
	@RequestMapping("/artist/name/{artistName}")
	public List<Song> byArtistName(@PathVariable("artistName") String artistName) {
		List<Song> songs = songService.findByArtistName(artistName);

		if (songs.isEmpty()) {
			throw new ArtistNotFoundException(artistName);
		} else {
			return songs;
		}
	}

	/**
	 * Fetch songs with the specified artist number.
	 * @param artistId
	 * @return A non-null, non-empty set of songs.
	 * @throws ArtistNotFoundException
	 *             If there are no matches at all.
	 */
	@Cacheable("songs")
	@RequestMapping("/artist/id/{artistId}")
	public List<Song> byArtistId(@PathVariable("artistId") Long artistId) {
		List<Song> songs = songService.findByArtistId(artistId);

		if (songs.isEmpty()) {
			throw new ArtistNotFoundException(artistId);
		} else {
			return songs;
		}
	}

	/**
	 * Fetch songs with the specified genre name.
	 * @param genreName
	 * @return A non-null, non-empty set of songs.
	 * @throws GenreNotFoundException
	 *             If there are no matches at all.
	 */
	@Cacheable("songs")
	@RequestMapping("/genre/name/{genreName}")
	public List<Song> byGenreName(@PathVariable("genreName") String genreName) {
		List<Song> songs = songService.findByGenreName(genreName);

		if (songs.isEmpty()) {
			throw new GenreNotFoundException(genreName);
		} else {
			return songs;
		}
	}

	/**
	 * Fetch songs with the specified genre number.
	 * @param genreId
	 * @return A non-null, non-empty set of songs.
	 * @throws GenreNotFoundException
	 *             If there are no matches at all.
	 */
	@Cacheable("songs")
	@RequestMapping("/genre/id/{genreId}")
	public Page<Song> byGenreId(@PathVariable("genreId") Long genreId) {
		return byGenreIdPageNumber(genreId, DEFAULT_PAGE);
	}

	/**
	 * Fetch songs with the specified genre number.
	 * @param genreId
	 * @param pageNumber
	 * @return A non-null, non-empty set of songs.
	 * @throws GenreNotFoundException
	 *             If there are no matches at all.
	 */
	@Cacheable("songs")
	@RequestMapping("/genre/id/{genreId}/{pageNumber}")
	public Page<Song> byGenreIdPageNumber(@PathVariable("genreId") Long genreId, @PathVariable("pageNumber") int pageNumber) {
		Page<Song> songs = songService.findByGenreId(genreId, pageNumber);

		if (!songs.hasContent()) {
			throw new GenreNotFoundException(genreId);
		} else {
			return songs;
		}
	}

	/**
	 * Fetch songs with the specified year.
	 * @param year
	 * @return A non-null, non-empty set of songs.
	 * @throws YearNotFoundException
	 *             If there are no matches at all.
	 */
	@Cacheable("songs")
	@RequestMapping("/year/{year}")
	public Page<Song> byYear(@PathVariable("year") int year) {
		return byYearPageNumber(year, DEFAULT_PAGE);
	}

	/**
	 * Fetch songs with the specified year.
	 * @param year
	 * @param pageNumber
	 * @return A non-null, non-empty set of songs.
	 * @throws YearNotFoundException
	 *             If there are no matches at all.
	 */
	@Cacheable("songs")
	@RequestMapping("/year/{year}/{pageNumber}")
	public Page<Song> byYearPageNumber(@PathVariable("year") int year, @PathVariable("pageNumber") int pageNumber) {
		Page<Song> songs = songService.findByYear(year, pageNumber);

		if (!songs.hasContent()) {
			throw new YearNotFoundException(year);
		} else {
			return songs;
		}
	}

	/**
	 * Fetch songs with the specified first letter from name.
	 *
	 * @param firstLetter
	 * @return A non-null, non-empty set of songs.
	 * @throws SongNotFoundException
	 *             If there are no matches at all.
	 */
	@Cacheable("songs")
	@RequestMapping("/letter/{firstLetter}/{pageNumber}")
	public Page<Song> byFirstLetter(@PathVariable("firstLetter") String firstLetter, @PathVariable("pageNumber") int pageNumber) {
		Page<Song> songs = songService.findByFirstLetter(firstLetter, pageNumber);

		if (!songs.hasContent()) {
			throw new SongNotFoundException("Starting with " + firstLetter +" and page " + pageNumber);
		} else {
			return songs;
		}
	}

	/**
	 * Fetch all songs.
	 *
	 * @return All songs.
	 */
	@Cacheable("songs")
	@RequestMapping("all")
	public List<Song> showAll() {
		return songService.findAll();
	}

	/**
	 * Fetch song count.
	 *
	 * @return Number of songs.
	 */
	@Cacheable("songs")
	@RequestMapping("count")
	public Long showCount() {
		return songService.count();
	}

}
