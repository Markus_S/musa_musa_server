package com.myqnapcloud.sonata.musa.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.myqnapcloud.sonata.musa.exceptions.GenreNotFoundException;
import com.myqnapcloud.sonata.musa.model.Genre;
import com.myqnapcloud.sonata.musa.service.GenreService;

/**
 * A RESTFul controller for accessing genre information.
 * 
 * @author Markus Sairanen
 */
@RestController
@RequestMapping("/genres")
public class GenreController {
	
	private GenreService genreService;
	
	@Autowired
	public GenreController(GenreService genreService) {
		this.genreService = genreService;
	}

	/**
	 * Fetch genre with the specified name.
	 * 
	 * @param genreName
	 * @return Genre if found.
	 * @throws GenreNotFoundException
	 *             If there are no matches at all.
	 */
	@Cacheable("genres")
	@RequestMapping("/name/{genreName}")
	public Genre byName(@PathVariable("genreName") String genreName) {
		Genre genre = genreService.findByname(genreName);
		
		if (genre == null) {
			throw new GenreNotFoundException(genreName);
		} else {
			return genre;
		}
	}
	
	/**
	 * Fetch genres with the specified first letter from name.
	 * 
	 * @param firstLetter
	 * @return A non-null, non-empty set of genres.
	 * @throws GenreNotFoundException
	 *             If there are no matches at all.
	 */
	@Cacheable("genres")
	@RequestMapping("/letter/{firstLetter}/{pageNumber}")
	public Page<Genre> byFirstLetter(@PathVariable("firstLetter") String firstLetter, @PathVariable("pageNumber") int pageNumber) {
		Page<Genre> genres = genreService.findByFirstLetter(firstLetter, pageNumber);
		
		if (!genres.hasContent()) {
			throw new GenreNotFoundException("Starting with " + firstLetter +" and page " + pageNumber);
		} else {
			return genres;
		}
	}
	
	/**
	 * Fetch genre with the specified number.
	 * 
	 * @param genreId
	 * @return Genre if found.
	 * @throws GenreNotFoundException
	 *             If there are no matches at all.
	 */
	@Cacheable("genres")
	@RequestMapping("/id/{genreId}")
	public Genre byGenreId(@PathVariable("genreId") Long genreId) {
		Genre genre = genreService.findbyId(genreId);
		
		if (genre == null) {
			throw new GenreNotFoundException(genreId);
		} else {
			return genre;
		}
	}

	/**
	 * Fetch all genres.
	 * 
	 * @return All genres.
	 */
	@Cacheable("artists")
	@RequestMapping("all")
	public List<Genre> showAll() {
		return genreService.findAll();
	}
	
	/**
	 * Fetch genre count.
	 * 
	 * @return Number of genres.
	 */
	@Cacheable("genres")
	@RequestMapping("count")
	public Long showCount() {
		return genreService.count();
	}
	
}
