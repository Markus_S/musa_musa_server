package com.myqnapcloud.sonata.musa.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.myqnapcloud.sonata.musa.exceptions.AlbumNotFoundException;
import com.myqnapcloud.sonata.musa.exceptions.ArtistNotFoundException;
import com.myqnapcloud.sonata.musa.model.Album;
import com.myqnapcloud.sonata.musa.service.AlbumService;

/**
 * A RESTFul controller for accessing album information.
 * 
 * @author Markus Sairanen
 */
@RestController
@RequestMapping("/albums")
public class AlbumController {
	
	private AlbumService albumService;
	
	@Autowired
	public AlbumController(AlbumService albumService) {
		this.albumService = albumService;
	}

	/**
	 * Fetch albums with the specified name.
	 * 
	 * @param albumName
	 * @return A non-null, non-empty set of albums.
	 * @throws AlbumNotFoundException
	 *             If there are no matches at all.
	 */
	@Cacheable("albums")
	@RequestMapping("/name/{albumName}")
	public List<Album> byName(@PathVariable("albumName") String albumName) {
		List<Album> albums = albumService.findByName(albumName);
		
		if (albums.isEmpty()) {
			throw new AlbumNotFoundException(albumName);
		} else {
			return albums;
		}
	}
	
	/**
	 * Fetch albums with the specified first letter from name.
	 * 
	 * @param firstLetter
	 * @return A non-null, non-empty set of albums.
	 * @throws AlbumNotFoundException
	 *             If there are no matches at all.
	 */
	@Cacheable("albums")
	@RequestMapping("/letter/{firstLetter}/{pageNumber}")
	public Page<Album> byFirstLetter(@PathVariable("firstLetter") String firstLetter, @PathVariable("pageNumber") int pageNumber) {
		Page<Album> albums = albumService.findByFirstLetter(firstLetter, pageNumber);
		
		if (!albums.hasContent()) {
			throw new AlbumNotFoundException("Starting with " + firstLetter +" and page " + pageNumber);
		} else {
			return albums;
		}
	}
	
	/**
	 * Fetch an album with the specified album number.
	 * 
	 * @param albumId
	 *            A numeric, album number.
	 * @return The album if found.
	 * @throws AlbumNotFoundException
	 *             If the number is not recognized.
	 */
	@Cacheable("albums")
	@RequestMapping("/id/{albumId}")
	public Album byAlbumId(@PathVariable("albumId") Long albumId) {
		Album album = albumService.findById(albumId);
		
		if (album == null) {
			throw new AlbumNotFoundException(albumId);
		} else {
			return album;
		}
	}
	
	/**
	 * Fetch an album with the specified artist number.
	 * 
	 * @param artistId
	 *            A numeric, artist number.
	 * @return The album if found.
	 * @throws ArtistNotFoundException
	 *             If the number is not recognized.
	 */
	@Cacheable("albums")
	@RequestMapping("/artist/id/{artistId}")
	public List<Album> byArtistId(@PathVariable("artistId") Long artistId) {
		List<Album> albums = albumService.findByArtistId(artistId);
		
		if (albums.isEmpty()) {
			throw new ArtistNotFoundException(artistId);
		} else {
			return albums;
		}
	}
	
	/**
	 * Fetch albums with the specified artist name.
	 * 
	 * @param artistName
	 * @return A non-null, non-empty set of albums.
	 * @throws ArtistNotFoundException
	 *             If there are no matches at all.
	 */
	@Cacheable("albums")
	@RequestMapping("/artist/name/{artistName}")
	public List<Album> byArtistName(@PathVariable("artistName") String artistName) {
		List<Album> albums = albumService.findByArtistName(artistName);
		
		if (albums.isEmpty()) {
			throw new ArtistNotFoundException(artistName);
		} else {
			return albums;
		}
	}
	
	/**
	 * Fetch all albums.
	 * 
	 * @return All albums.
	 */
	@Cacheable("albums")
	@RequestMapping("all")
	public List<Album> showAll() {
		return albumService.findAll();
	}
	
	/**
	 * Fetch album count.
	 * 
	 * @return Number of albums.
	 */
	@Cacheable("albums")
	@RequestMapping("count")
	public Long showCount() {
		return albumService.count();
	}

}
