package com.myqnapcloud.sonata.musa.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.myqnapcloud.sonata.musa.exceptions.AlbumNotFoundException;
import com.myqnapcloud.sonata.musa.exceptions.ArtistNotFoundException;
import com.myqnapcloud.sonata.musa.model.Artist;
import com.myqnapcloud.sonata.musa.service.ArtistService;

/**
 * A RESTFul controller for accessing artist information.
 * 
 * @author Markus Sairanen
 */
@RestController
@RequestMapping("/artists")
public class ArtistController {
	
	private ArtistService artistService;
	
	@Autowired
	public ArtistController(ArtistService artistService) {
		this.artistService = artistService;
	}

	/**
	 * Fetch artist with the specified name.
	 * 
	 * @param artistName
	 * @return Artist if found.
	 * @throws ArtistNotFoundException
	 *             If there are no matches at all.
	 */
	@Cacheable("artists")
	@RequestMapping("/name/{artistName}")
	public Artist byName(@PathVariable("artistName") String artistName) {
		Artist artist = artistService.findByname(artistName);
		
		if (artist == null) {
			throw new ArtistNotFoundException(artistName);
		} else {
			return artist;
		}
	}
	
	/**
	 * Fetch albums with the specified first letter from name.
	 * 
	 * @param firstLetter
	 * @return A non-null, non-empty set of albums.
	 * @throws AlbumNotFoundException
	 *             If there are no matches at all.
	 */
	@Cacheable("artists")
	@RequestMapping("/letter/{firstLetter}/{pageNumber}")
	public Page<Artist> byFirstLetter(@PathVariable("firstLetter") String firstLetter, @PathVariable("pageNumber") int pageNumber) {
		Page<Artist> artists = artistService.findByFirstLetter(firstLetter, pageNumber);
		
		if (!artists.hasContent()) {
			throw new ArtistNotFoundException("Starting with " + firstLetter +" and page " + pageNumber);
		} else {
			return artists;
		}
	}
	
	/**
	 * Fetch artist with the specified number.
	 * 
	 * @param artistId
	 * @return Artist if found.
	 * @throws ArtistNotFoundException
	 *             If there are no matches at all.
	 */
	@Cacheable("artists")
	@RequestMapping("/id/{artistId}")
	public Artist byArtistId(@PathVariable("artistId") Long artistId) {
		Artist artist = artistService.findById(artistId);
		
		if (artist == null) {
			throw new ArtistNotFoundException(artistId);
		} else {
			return artist;
		}
	}

	/**
	 * Fetch all artists.
	 * 
	 * @return All artists.
	 */
	@Cacheable("artists")
	@RequestMapping("all")
	public List<Artist> showAll() {
		return artistService.findAll();
	}
	
	/**
	 * Fetch artist count.
	 * 
	 * @return Number of artists.
	 */
	@Cacheable("artists")
	@RequestMapping("count")
	public Long showCount() {
		return artistService.count();
	}

}
